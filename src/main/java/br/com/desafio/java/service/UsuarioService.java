package br.com.desafio.java.service;

import java.util.List;

import br.com.desafio.java.domain.Message;
import br.com.desafio.java.domain.Usuario;

public interface UsuarioService {
	List<Usuario> listar();
	Message cadastrar(Usuario usuario);
}
