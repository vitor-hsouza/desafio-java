package br.com.desafio.java.domain;

public class Error {
	private String message;
	private String detalhe;

	public Error() {
		super();
	}

	public Error(String message) {
		super();
		this.message = message;
	}

	public Error(String message, String detalhe) {
		super();
		this.message = message;
		this.detalhe = detalhe;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getDetalhe() {
		return detalhe;
	}

	public void setDetalhe(String detalhe) {
		this.detalhe = detalhe;
	}

}
